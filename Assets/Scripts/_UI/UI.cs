﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UI : MonoBehaviour {

	public static Action startDelegate;
	public static Action restartDelegate;

	public GameObject endPanel;
	public GameObject gamePanel;
	public Button next;
	public InputField windX;
	public InputField windY;
	public Text timer;
	public Text score;
	public Text topScore;
	public Text stateText;

	public const int SCORE_MULTIPLIER = 7777;
	const int COUNTDOWN = 3;

	void Awake() {
		LocalStorage.Init();
	}

	void Start() {
		Lander.Instance.endDelegate += EndGame;
		windX.text = Wind.Instance.windforce.x.ToString();
		windY.text = Wind.Instance.windforce.y.ToString();
	}

	public void UpdateWindX(string x) {
		Wind.Instance.windforce.x = x.TryFloatParse() ?? 0f;
		if(x.TryFloatParse() == null) {
			windX.text = 0.ToString();
		}
	}

	public void UpdateWindY(string y) {
		Wind.Instance.windforce.y = y.TryFloatParse() ?? 0f;
		if(y.TryFloatParse() == null) {
			windY.text = 0.ToString();
		}
	}

	public void RestartGame() { 
		restartDelegate.Fire();
		PlayLevel();
	}

	public void StartGame() { 
		startDelegate.Fire();
		PlayLevel ();
	}

	void EndGame(Lander.State state) {
		gamePanel.SetActive(false);
		endPanel.SetActive(true);
		Lander.Instance.rb.isKinematic = true;
		DisplayScore(state);
	}

	public void PauseGame(bool pause) {
		Lander.Instance.rb.isKinematic = pause;
		if(!pause){
			Lander.Instance.rb.WakeUp();
		}
	}

	void PlayLevel () {
		Lander.Instance.Reset ();
		Lander.Instance.gameObject.SetActive (true);
		StartCoroutine (TimerRoutine ());
	}

	public void Quit() {
		Application.Quit();
	}

	void DisplayScore(Lander.State state) {
		var s = Lander.Instance.remainingHealth * SCORE_MULTIPLIER;
		score.text = "Score: " + (int)Mathf.Max(0, s);
		var storagePref = "Score_" + Levels.Instance.difficulty +"_" + Levels.Instance.level;
		if(s > PlayerPrefs.GetFloat(storagePref) && state == Lander.State.Successful) {
			PlayerPrefs.SetFloat(storagePref, s);
		}
		score.gameObject.SetActive(state == Lander.State.Successful);
		next.gameObject.SetActive(state == Lander.State.Successful);
		topScore.text = "Top Score: " + (int)PlayerPrefs.GetFloat(storagePref);
		stateText.text = state + " landing";
	}

	readonly YieldInstruction yielder = new WaitForSeconds(1);
	IEnumerator TimerRoutine() {
		timer.gameObject.SetActive(true);
		for(int i = COUNTDOWN; i > 0; i--) {
			timer.text = i.ToString();
			yield return yielder;
		}
		timer.gameObject.SetActive(false);
		Lander.Instance.rb.isKinematic = false;
	}

	[ContextMenu("DeleteLocalStorage")]
	void DeleteLocalStorage() {
		PlayerPrefs.DeleteAll();
		Debug.Log("Deleting local storage...");
	}
}

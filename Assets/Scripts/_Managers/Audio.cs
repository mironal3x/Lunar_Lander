﻿using UnityEngine;

public class Audio : MonoBehaviour {

	public AudioSource soundSource;
	public AudioSource musicSource;

	void Start() {
		soundSource.volume = LocalStorage.Sound;
		musicSource.volume = LocalStorage.Music;

		Lander.Instance.engineFireDelegate += EngineSound;
	}

	public void SetSound(float value) {
		soundSource.volume = value;
		LocalStorage.Sound = value;
	}

	public void SetMusic(float value) {
		musicSource.volume = value;
		LocalStorage.Music = value;
	}


	float startTime;
	bool isPlaying { get { return Time.time < startTime + soundSource.clip.length; } }

	void EngineSound() {
		if(!isPlaying) {
			soundSource.PlayOneShot(soundSource.clip);
			startTime = Time.time;
		}
	}
}

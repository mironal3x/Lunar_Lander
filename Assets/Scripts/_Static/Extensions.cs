﻿using System;

public static class Extensions { }
	
public static class ActionExtensions {

	public static void Fire(this Action action) {
		if(action != null) {
			action();
		}
	}

	public static void Fire<T>(this Action<T> action, T t) {
		if(action != null) {
			action(t);
		}
	}

	public static void Fire<T,U,V>(this Action<T,U,V> action, T t, U u, V v) {
		if(action != null) {
			action(t, u, v);
		}
	}
}

public static class StringExtensions {

	public static bool EmptyOrNull(this string str) {
		return string.IsNullOrEmpty(str);
	}

	public static float? TryFloatParse(this string str) {
		var parsedFloat = 0f;
		if(float.TryParse(str, out parsedFloat)) {
			return parsedFloat;
		}
		else {
			return null;
		}
	}
}

public static class ArrayExtensions {

	public static void ForEach<T>(this T[] array, Action<T> action) {
		Array.ForEach(array, action);
	}

	public static T Random<T>(this T[] array) {
		return array[UnityEngine.Random.Range(0, array.Length)];
	}
}

public static class ObjectExtensions {

	public static void Destroy(this UnityEngine.Object go, float delay = 0) {
		UnityEngine.Object.Destroy(go, delay);
	}
}

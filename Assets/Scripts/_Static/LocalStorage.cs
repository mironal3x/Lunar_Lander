﻿using UnityEngine;

public static class LocalStorage {

	const float DEFAULT_MUSIC = 0.5f;
	const float DEFAULT_SOUND = 0.5f;

	static float sound;
	public static float Sound {
		get { return sound; } 
		set {
			sound = value;
			PlayerPrefs.SetFloat("sound", sound);
			PlayerPrefs.Save();
		}
	}

	static float music;
	public static float Music {
		get { return music; } 
		set {
			music = value;
			PlayerPrefs.SetFloat("music", music);
			PlayerPrefs.Save();
		}
	}

	public static void Init() {
		if(!PlayerPrefs.HasKey("sound")) {
			CreateData();
			Debug.Log("Creating data...");
		}
		LoadData();
	}
	
	static void CreateData() {
		PlayerPrefs.SetFloat("sound", DEFAULT_SOUND);
		PlayerPrefs.SetFloat("music", DEFAULT_MUSIC);
	}

	static void LoadData() {
		Sound = PlayerPrefs.GetFloat("sound");
		Music = PlayerPrefs.GetFloat("music");
	}
}

﻿using UnityEngine;
using System.Collections.Generic;
using Rand = UnityEngine.Random;

public class Wind : Singleton<Wind>{

	public Vector2 windforce;
	public Transform windArrow;

	readonly List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();

	public void Subscribe(Rigidbody2D rBody) {
		rigidbodies.Add(rBody);
	}

	void FixedUpdate () {
		if(Lander.Instance.rb.isKinematic) return;

		windArrow.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(windforce.y, windforce.x) * Mathf.Rad2Deg);
		foreach(var rb in rigidbodies) {
			rb.AddForce(windforce);
		}
	}
}

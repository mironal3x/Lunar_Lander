﻿using UnityEngine;
using System;

public class Levels : Singleton<Levels> {

	public static Action<int, int, float> parseDelegate;
	public static Action<int> difficultyDelegate;

	public GameObject[] levelDesigns;

	public int level;
	public int difficulty;

	void Start() {
		level = 0;

		UI.startDelegate += () => {
			Debug.Log("Start: " + level);
			for(int i = 0; i < levelDesigns.Length; i++) {
				levelDesigns[i].SetActive(i == level);
			}
			if(level == levelDesigns.Length) {
				level = 0;
				levelDesigns.ForEach(l => l.SetActive(false));
				levelDesigns[level].SetActive(true);
				difficultyDelegate.Fire(difficulty);
				difficulty++;
			}
			parseDelegate.Fire(level, difficulty, Lander.Instance.remainingHealth * UI.SCORE_MULTIPLIER);
			level++;
		};
	}
}
